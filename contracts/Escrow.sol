pragma solidity ^ 0.4 .24;

/*@title Escrow Dapp contract 
*@dev NO OPT, DEMO USE ONLY
*/

contract Ownable {

    address public owner;

    constructor() public {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
}

contract Juror is Ownable {

    struct judge {
      
        uint listPointer;
        bool voted;
    }
    
    address[] entityList;
    mapping(address => judge) public judges;
    
     function isOR(address wallet) public view returns(bool isIndeed) {
         if (entityList.length == 0) return false;
         return (entityList[judges[wallet].listPointer] == wallet);
     }
     
      function isVoted(address wallet) public view returns(bool) {
         return (judges[wallet].voted == true);
     }
    
    
     function getJuror() public view returns(address[]) {
         return entityList;
     }
     
     function beJudge() public {
         if (isOR(msg.sender)) revert();
        
         judges[msg.sender].voted = false;
         judges[msg.sender].listPointer = entityList.push(msg.sender) - 1;
     }
     
     function addJudge(address[] _list) public {
       
         for(uint i=0;i<_list.length;i++){
        
         judges[_list[i]].voted = false;
         judges[_list[i]].listPointer = entityList.push(_list[i]) - 1;
         }
     }
   
}



contract Escrow is Juror {

    struct example {
        address A;
        address B;
        uint ETH;
        uint voteA;
        uint voteB;
        bool voted;
        string description;
        uint listPointer;
        address who;
       
    }
    
    uint[] exampleList;
    mapping(uint => example) public examples;

    constructor() public {
    
    }

    function newExample(address _A, address _B, string _descrip) public payable {
        require(msg.value > 0);

        examples[1].A = _A;
        examples[1].B = _B;
        examples[1].ETH = msg.value;
        examples[1].voted = false;
        examples[1].description = _descrip;
        examples[1].listPointer = exampleList.push(1) - 1;
        
    }
    
    function vote(uint _vote) public {
        
    require(judges[msg.sender].voted == false);
    require(examples[1].voted == false);
    require(examples[1].A != msg.sender && examples[1].B != msg.sender);
    require(examples[1].voteA + examples[1].voteB <=3);
    
       if (!isOR(msg.sender)) revert();
       
       if(_vote == 0){examples[1].voteA = examples[1].voteA + 1 ;}
       else if(_vote == 1){examples[1].voteB = examples[1].voteB + 1;}
       else if(_vote > 2){return revert();}
       
       judges[msg.sender].voted = true;
       
         if(examples[1].voteA + examples[1].voteB == 3) {
        decide();
         }
        
    }
    
    function decide() internal {
        
        require(examples[1].voted == false);
        
        if(examples[1].voteA > examples[1].voteB) {
            examples[1].who = examples[1].A;
            examples[1].A.transfer(examples[1].ETH);
            
        } else {
           examples[1].who = examples[1].B;
           examples[1].B.transfer(examples[1].ETH);
           
        }
        
        examples[1].voted = true;
    }
    
    function reset() public onlyOwner {
        examples[1].A = 0;
        examples[1].B = 0;
        examples[1].voteA = 0;
        examples[1].voteB = 0;
        examples[1].ETH = 0;
        examples[1].description = "";
        examples[1].voted = false;
        examples[1].who = 0;
        for(uint i=0; i<entityList.length; i++) {
            judges[entityList[i]].voted = false;
        }
        delete entityList;
        
        
    }


}