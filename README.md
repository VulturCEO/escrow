# Democratic Escrow Demo Dapp

Requerimientos
------------

  * Metamask
  * Google Chrome

Instrucciones
------------

1. Crea tu cuenta de metamask con la siguiente frase semilla:


```label oak ozone ecology valve sponsor wheat indoor crucial talk inquiry net```

La frase semilla contiene 6 Billeteras diferentes cargadas con ETH de la red Ropsten.

2. Conéctate a la red Ropsten en Metamask

3. CREAR: Utilizando la primera Wallet "0xd79296334085Ab7bf91ff4AC9868Bcf6545d2c29" Haz click en "Crear" y espera la confirmación, si es necesario refresca la página una vez finalizado, verás los resultados al lado derecho.

4. AGREGAR JURADO: Ahora haz click en "Agregar", estarás agregando el jurado que incluye 5 billeteras, esto es necesario para poder votar.

5. VOTAR: Puedes votar unicamente con las billeteras listadas, para hacerlo debes seleccionar "Si" o "No" e ir cambiado de wallet de metamask... RECUERDA, debes refrescar la página cada vez que cambies de cuenta.

6. EJECUTAR: Cuando la votación alcance su QUORUM haz click en "ejecutar decisión" esto no es automático por temas de prueba, puedes ser ejecutada sin importar la cantidad de votos, en caso de paridad no se ejecutará.
Una vez finalizada metamask te mostrará la confirmación en Etherscan, 1 eth será enviado a la cuenta correspondiente según el resultado.

