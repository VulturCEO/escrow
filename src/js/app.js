$(function () {
  $(window).load(function () {
    App.init();

  });

  App = {
    web3Provider: null,
    contracts: {},

    init: function () {
      return App.initWeb3();
    },

    initWeb3: function () {
      if (typeof web3 !== 'undefined') {
        App.web3Provider = web3.currentProvider;
        web3 = new Web3(web3.currentProvider);
      } else {
        App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:9545');
        web3 = new Web3(App.web3Provider);
      }

      return App.initContract();
    },

    initContract: function () {
      $.getJSON('Escrow.json', function (data) {
        var EscrowArtifact = data;
        App.contracts.Escrow = TruffleContract(EscrowArtifact);

        App.contracts.Escrow.setProvider(App.web3Provider);
        App.getJuror();
        return App.getExample();

      });

      return App.bindEvents();
    },

    bindEvents: function () {
      $(document).on('click', '#transferButton', App.handleTransfer);
      $(document).on('click', '#create', App.handleCreate);
      $(document).on('click', '#add', App.addJudge);
      $(document).on('click', '#voteYes', App.voteYes);
      $(document).on('click', '#voteNo', App.voteNo);
      $(document).on('click', '#reset', App.reset);
      $(document).on('click', '#decide', App.decide);
      $(document).on('click', '#toVote', App.isOR);
      $(document).on('click', '#fin', App.getJuror);





    },

    handleTransfer: function (event) {
      event.preventDefault();

      var amount = parseInt($('#TTTransferAmount').val());
      var toAddress = $('#TTTransferAddress').val();

      console.log('Transfer ' + amount + ' TT to ' + toAddress);

      var EscrowInstance;

      web3.eth.getAccounts(function (error, accounts) {
        if (error) {
          console.log(error);
        }

        var account = accounts[0];

        App.contracts.Escrow.deployed().then(function (instance) {
          EscrowInstance = instance;

          return EscrowInstance.transfer(toAddress, amount, {
            from: account
          });
        }).then(function (result) {
          alert('Transfer Successful!');
         
        }).catch(function (err) {
          console.log(err.message);
        });
      });
    },

    handleCreate: function (event) {
      event.preventDefault();


      if ($('#check').is(':checked') === false) {
        return alert('Debes aceptar las condiciones.');
      }
      var amount = parseInt($('#amount').val()) * 1000000000000000000;
      var Awallet = $('#Awallet').val();
      var Bwallet = $('#Bwallet').val();
      var description = $('#description').val();

      var EscrowInstance;
      web3.eth.getAccounts(function (error, accounts) {
        if (error) {
          console.log(error);
        }
        var account = accounts[0];

        App.contracts.Escrow.deployed().then(function (instance) {
          EscrowInstance = instance;
          $('#loading').modal();
          return EscrowInstance.newExample(Awallet, Bwallet, description, {
            from: account,
            value: amount,
          });
        }).then(function (result) {
          $('#loading').modal('hide');

          alert('Felicidades, haz creado el contrato, avanza al siguiente paso.');
          return App.getExample();
        }).catch(function (err) {
          console.log(err.message);
        });
      })
    },

     addJudge: function (event) {
       event.preventDefault();

       var list = [$('#list0').val(), $('#list1').val(), $('#list2').val(), $('#list3').val(), $('#list4').val()];

       var EscrowInstance;
       web3.eth.getAccounts(function (error, accounts) {
         if (error) {
           console.log(error);
         }
         var account = accounts[0];

         App.contracts.Escrow.deployed().then(function (instance) {
           EscrowInstance = instance;
           $('#loading').modal();

           return EscrowInstance.addJudge(list, {
             from: account,
           });
         }).then(function (result) {
           $('#loading').modal('hide');
           alert('Juror Added!');
           return App.getJuror();
         }).catch(function (err) {
           console.log(err.message);
         });
       })
     },

     voteYes: function (event) {
       event.preventDefault();


       var EscrowInstance;
       web3.eth.getAccounts(function (error, accounts) {
         if (error) {
           console.log(error);
         }
         var account = accounts[0];
         $('#loading').modal();

         App.contracts.Escrow.deployed().then(function (instance) {
           EscrowInstance = instance;

           return EscrowInstance.vote(1, {
             from: account,
           });
         }).then(function (result) {
           $('#loading').modal('hide');

           alert('Vote Yes is ready!');
return App.getExample();
         }).catch(function (err) {
           console.log(err.message);
         });
       })
     },

      voteNo: function (event) {
        event.preventDefault();


        var EscrowInstance;
        web3.eth.getAccounts(function (error, accounts) {
          if (error) {
            console.log(error);
          }
          var account = accounts[0];

          App.contracts.Escrow.deployed().then(function (instance) {
            EscrowInstance = instance;
            $('#loading').modal();

            return EscrowInstance.vote(0, {
              from: account,
            });
          }).then(function (result) {
            $('#loading').modal('hide');

            alert('Vote No is ready!');
            return App.getExample();
          }).catch(function (err) {
            console.log(err.message);
          });
        })
      },

    reset: function (event) {
      event.preventDefault();


      var EscrowInstance;
      web3.eth.getAccounts(function (error, accounts) {
        if (error) {
          console.log(error);
        }
        var account = accounts[0];

        App.contracts.Escrow.deployed().then(function (instance) {
          EscrowInstance = instance;

          return EscrowInstance.reset( {
            from: account,
          });
        }).then(function (result) {
          alert('All clean!');
          return App.getExample();
        }).catch(function (err) {
          console.log(err.message);
        });
      })
    },

    decide: function (event) {
      event.preventDefault();

      var EscrowInstance;
      web3.eth.getAccounts(function (error, accounts) {
        if (error) {
          console.log(error);
        }
        var account = accounts[0];

        App.contracts.Escrow.deployed().then(function (instance) {
          EscrowInstance = instance;

          return EscrowInstance.decide({
            from: account,
          });
        }).then(function (result) {
          alert('Decided, check the TX Hash on metamask!');
          return App.getExample();
        }).catch(function (err) {
          console.log(err.message);
        });
      })
    },

    isOR: function () {

      var EscrowInstance;
      web3.eth.getAccounts(function (error, accounts) {
        if (error) {
          console.log(error);
        }
      App.contracts.Escrow.deployed().then(function (instance) {
        EscrowInstance = instance;

        return EscrowInstance.isOR(accounts[0]);
      }).then(function (result) {
        console.log('ES JURADO?: ' +result);
        $('#isJuror').modal();
if (result === true) {
  return $('#isJurorText').html('Felicidades, haz sido seleccionado como jurado');
} else {
$('#voteYes').attr('disabled', true);
$('#voteNo').attr('disabled', true);

  return $('#isJurorText').html('Lo sentimos, no eres jugado de este proceso.');


}

      }).catch(function (err) {
        console.log(err.message);
      });
    });
    },

    getJuror: function () {

      var EscrowInstance;

      App.contracts.Escrow.deployed().then(function (instance) {
        EscrowInstance = instance;

        return EscrowInstance.getJuror();
      }).then(function (result) {

         if (result.length === 0) {
           $('#fin2').attr('disabled', true);
            $('#fin3').attr('disabled', true);

           $('#jurorText').html('Aún no puedes avanzar, el jurado no ha sido inscrito, vuelve al paso anterior.')

         } else {
            $('#fin2').attr('disabled', false);
            $('#fin3').attr('disabled', false);
           $('#jurorText').html('Genial, es click en Panel de Votación para continuar.')

         }
        console.log(result);
        
        for(i=0;i<5;i++) {
        $('#listResult'+i).text(result[i]);
        }
      }).catch(function (err) {
        console.log(err.message);
      });
    },

    getExample: function () {

      var EscrowInstance;

      App.contracts.Escrow.deployed().then(function (instance) {
        EscrowInstance = instance;

        return EscrowInstance.examples(1);
      }).then(function (result) {
        juror = result;

        if (result[0] === '0x0000000000000000000000000000000000000000') {
         return $('#next').attr('disabled', true);

        } else {
        $('#next').attr('disabled', false);

        }
        $('#A').text(result[0]);
        $('#B').text(result[1]);
        $('#no').text(result[3]);
        $('#yes').text(result[4]);


         web3.eth.getAccounts(function (error, accounts) {
               if (error) {
                 console.log(error);
               }
           
        if (result[8] === accounts[0]) {
          $('#win').modal();
        }

        console.log(result[8]);
           })
        if(result[5] == false) {
          $('#status').text('Abierta');
        } else {
          $('#finished').modal();
          $('#toVote').attr('disabled', true);
          $('#status').text('Finalizada');
        }
       



        $('#descripResult').text(result[6]);

        console.log(result);

        $('#juror').text(juror);
      }).catch(function (err) {
        console.log(err.message);
      });
    },

   
  

  };


});